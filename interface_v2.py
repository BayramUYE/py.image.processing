from Tkinter import *
from tkFileDialog import *
import numpy as np
import cv2
import Image, ImageTk
from matplotlib import pyplot as plt
import sys

#Setup Gui 
pencere = Tk()
frame1 = Frame(pencere, width = 300,height=100 )
frame1.pack(side = LEFT)
frame2 = Frame (pencere,width = 300,height =100)
frame2.pack(side = RIGHT)
frame3 = Frame (pencere)
frame3.pack(side=TOP)
frame4 = Frame(pencere, width =100,height=50 )
frame4.pack(side=BOTTOM)
pencere.title("Bayram Uye Image Processing Interfaces")
pencere.geometry("950x550")
#pencere.config(background ="#000000")
lmain = Label(frame4)
lmain.grid(row=0, column=0)
cap = cv2.VideoCapture(0)






def show_frame():
    _, frame = cap.read()
    frame = cv2.flip(frame, 1)
    cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
    img = Image.fromarray(cv2image)
    imgtk = ImageTk.PhotoImage(image=img)
    lmain.imgtk = imgtk
    lmain.configure(image=imgtk)
    lmain.after(10, show_frame)
    
   
    
def takephoto():
    _, frame = cap.read()
    frame = cv2.flip(frame, 1)
    cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
    img = Image.fromarray(cv2image)
    imgtk = ImageTk.PhotoImage(image=img)
    lmain.imgtk = imgtk
    lmain.configure(image=imgtk)
    lmain.after(10, show_frame)
    #cv2.imshow(10,show_frame)   
    
 
        
    
    

def blackwhite():
    _, frame2 = cap.read()
    cv2image1 = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)
    img1 = Image.fromarray(cv2image1)
    cv2.imshow('img1',cv2image1)

def facedetect():

    _, imgf = cap.read()

    while(1):
        
        face_cascade = cv2.CascadeClassifier('C:\Users\Bayram Efe\Desktop\opencv\sources\data\haarcascades\haarcascade_frontalface_default.xml')
        eye_cascade = cv2.CascadeClassifier('C:\Users\Bayram Efe\Desktop\opencv\sources\data\haarcascades\haarcascade_eye.xml')

        _, imgf = cap.read()
        gray1 = cv2.cvtColor(imgf, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray1, 1.3, 5)
        for (x,y,w,h) in faces:
            imga = cv2.rectangle(imgf,(x,y),(x+w,y+h),(255,0,0),2)
            roi_gray = gray1[y:y+h, x:x+w]
            roi_color = imga[y:y+h, x:x+w]
            eyes = eye_cascade.detectMultiScale(roi_gray)
            for (ex,ey,ew,eh) in eyes:
                cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)

        cv2.imshow('img',imgf)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break


def canny ():
    
    _, imgg = cap.read()

    while(1 ):
    
        _, imgg = cap.read()
        hsv = cv2.cvtColor(imgg, cv2.COLOR_BGR2RGB)
    
        lower_red = np.array([30,150,50])
        upper_red = np.array([255,255,180])
    
        mask = cv2.inRange(hsv, lower_red, upper_red)
        res = cv2.bitwise_and(imgg,imgg, mask= mask)

        kenar = cv2.Canny(imgg,100,200)
        cv2.imshow('Kenarlari',kenar)
        if cv2.waitKey(1) & 0xFF == 27: 
            cv2.destroyAllWindows()
            break
    
        
            


        
def dosyasec():
    dosyaal=askopenfilename()
    if dosyaal:
        yazi2.config(text=dosyaal)
    else:
        yazi2.config(text="Seilen Dosya: Dosya Seilmedi")

yazi=Label(frame3,text="Img Seciniz").pack()
yazi2=Label(frame3,text="Secilen Dosya: Henz dosya Secilmedi").pack()





Button(frame1,text ="   Capture     ",command = takephoto ).pack(side = BOTTOM)
Button(frame1,text ="    Canny      ",command = canny     ).pack(side = BOTTOM)
Button(frame1,text =" FaceDetect    ",command = facedetect).pack(side = BOTTOM)
Button(frame1,text =" BlackorWhite  ",command = blackwhite).pack(side = BOTTOM)
Button(frame1,text ="   ").pack(side = BOTTOM)
Button(frame2,text ="   ").pack(side = TOP)
Button(frame2,text ="  ").pack(side = TOP)
Button(frame3,text ="OpenFile     ",command=dosyasec).pack(side=LEFT)
Button(frame3,text ="ShowCamera   ",command=show_frame).pack(side = LEFT)
#grid(padx=750, pady=350)
#yazi = Label(text = "ImageProcessingInterface").grid(padx = 0, pady=0)



pencere.mainloop()
